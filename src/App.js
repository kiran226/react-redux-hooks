import './App.css';
import { Provider } from 'react-redux';
import store from './redux/store'
import CakeContainer from './components/CakeContainer';
import HooksCakeContainer from './components/HooksCakeContainer';
import ChoclateContainer from './components/ChoclateContainer';


function App() {
  return (
    <Provider store={store}>
      <div className="App">
          <h1>Lord Vinayaka</h1>
          <h2>Lord Subramanyam</h2>

          <HooksCakeContainer />
          <CakeContainer/>

          <ChoclateContainer/>

          
      </div>
    </Provider>  
  );
}

export default App;
