import React from 'react'
import { connect } from 'react-redux'
import buyChoclate from '../redux/choclate/choclateActions'

function ChoclateContainer(props) {
    return (
        <div>
            <h1>Number of Choclates : {props.numberOfChoclates} </h1>
            <button onClick={props.buyChoclate} >BUY CHOCLATE</button>
        </div>
    )
}

const mapStateToProps = state =>{
    return {
        numberOfChoclates : state.choclate.numberOfChoclates
    }
}

const mapDispatchToProps = dispatch =>{
    return {
        buyChoclate : ()=>dispatch(buyChoclate())
    }
}

export default connect(mapStateToProps,mapDispatchToProps)  (ChoclateContainer)
