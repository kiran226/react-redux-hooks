import { BUY_CHOCLATE } from "./choclateTypes"

const buyChoclate = () =>{
    return {
        type : BUY_CHOCLATE
    }
}

export default buyChoclate;