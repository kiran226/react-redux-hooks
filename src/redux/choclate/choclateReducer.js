import { BUY_CHOCLATE } from "./choclateTypes"

const initialState = {
    numberOfChoclates : 20
}

const choclateReducer = ( state = initialState, action) =>{
    switch(action.type){
        case BUY_CHOCLATE :
            return {
                ...state,
                numberOfChoclates : state.numberOfChoclates-1
            }
        default :
            return state    
    }
}

export default choclateReducer;