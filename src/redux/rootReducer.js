import { combineReducers} from 'redux';
import cakeReducer from './cake/cakeReducer';
import choclateReducer from './choclate/choclateReducer';

const rootReducer = combineReducers({
    cake : cakeReducer,
    choclate : choclateReducer
})

export default rootReducer